import {
  Body,
  Controller,
  Delete,
  Get,
  Param,
  Post,
  Put,
  Query,
} from '@nestjs/common';
import { RequestTimeout } from 'synapse.bff.core';
import { ItemDto, TaskEntity } from '../dto';
import { MessageEntity } from '../dto/messageEntity';
import { ItemService } from '../services/item.service';

@Controller('item')
export class ItemController {
  private start: number;

  constructor(private readonly itemService: ItemService) {
    this.start = Date.now();
  }
  @Post('create')
  create(@Body() itemDto: ItemDto) {
    return this.itemService.addItem(itemDto);
  }

  @RequestTimeout('query')
  @Get('getdatetime')
  async getdatetime() {
    return Date.now();
  }

  // @RequestTimeout('query')
  // @Get(':id')
  // getdatetimeparam(@Param('id') id): string {
  //   return id + ' ' + Date.now().toString();
  // }

  @RequestTimeout('query')
  @Get('getdatetimequery')
  async getdatetimequery(@Query('id') id: string) {
    return id + ' ' + Date.now().toString();
  }

  @RequestTimeout('query')
  @Get('getdatetimequery2')
  async getdatetimequery2(
    @Query('empid') empid: number,
    @Query('name') name: string,
  ) {
    return empid + ' ' + name + Date.now().toString();
  }
  @RequestTimeout('query')
  @Get(':id')
  async findone(@Param('id') id) {
    return await this.itemService.getItems(id);

  }
  @RequestTimeout('query')
  @Get('getlargejson')
  async getlargejson() {
    return await this.itemService.getlargejson();
  }
  @RequestTimeout('query')
  @Put('fetchOracleQueryData')
  async fetchOracleQueryData() {
    return this.itemService.fetchOracleQueryData();
  }
  @RequestTimeout('query')
  @Put('fetchOracleStoredProcData')
  async fetchOracleStoredProcData(@Body() taskEntity: TaskEntity) {
    return this.itemService.fetchOracleStoredProcReturnCursor(taskEntity);
  }

  @RequestTimeout('query')
  @Put('fetchPGStoredProcData')
  async fetchPGStoredProcData() {
    return this.itemService.fetchPGStoredProcData();
  }

  @RequestTimeout('query')
  @Put('fetchPGQueryData')
  async fetchPGQueryData() {
    return this.itemService.fetchPGQueryData();
  }
  @Put('update')
  update(@Body() itemDto: ItemDto) {
    return this.itemService.updateItem(itemDto);
  }
  @Put('publish')
  publish(@Body() messageEntity: MessageEntity) {
    return this.itemService.publish(messageEntity);
  }
  @Put('publishTopic')
  publishTopic(@Body() messageEntity: MessageEntity) {
    return this.itemService.publishTopic(messageEntity);
  }
  @Put('publishDirect')
  publishDirect(@Body() messageEntity: MessageEntity) {
    return this.itemService.publishDirect(messageEntity);
  }
  @Put('newTask')
  newTask(@Body() messageEntity: MessageEntity) {
    return this.itemService.newTask(messageEntity);
  }
  @Delete(':code')
  remove(@Param('code') code: string) {
    return this.itemService.deleteItem(code);
  }
}
