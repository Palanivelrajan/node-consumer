import { Test, TestingModule } from '@nestjs/testing';
import { ItemService } from '../services/item.service';
import { ItemController } from './item.controller';

describe('Item Controller', () => {
  let controller: ItemController;
  let itemService: ItemService;
  const messageEntity = {
    Name: 'name',
    Message: 'message',
  };
  const itemDto = {
    Code: 'string',
    Name: 'string',
    Price: 1,
    Category: 'string',
  };
  beforeEach(async () => {
    const module = await Test.createTestingModule({
      imports: [TestingModule],
      controllers: [ItemController],
      providers: [
        {
          provide: ItemService,
          useValue: {
            addItem: jest.fn(),
            getItems: jest.fn(),
            fetchOracleQueryData: jest.fn(),
            fetchOracleStoredProcData: jest.fn(),
            fetchPGStoredProcData: jest.fn(),
            fetchPGQueryData: jest.fn(),
            updateItem: jest.fn(),
            publish: jest.fn(),
            publishTopic: jest.fn(),
            publishDirect: jest.fn(),
            newTask: jest.fn(),
            deleteItem: jest.fn(),
          },
        },
      ],
    }).compile();

    controller = module.get<ItemController>(ItemController);
    itemService = module.get<ItemService>(ItemService);
  });

  it('should be defined', () => {
    expect(controller).toBeDefined();
  });
  it('create', () => {
    controller.create(itemDto);
    expect(itemService.addItem).toHaveBeenCalledWith(itemDto);
  });
  it('findOne', () => {
    controller.findOne();
    expect(itemService.getItems).toHaveBeenCalledWith('');
  });
  it('create', () => {
    controller.fetchOracleQueryData();
    expect(itemService.fetchOracleQueryData).toHaveBeenCalled();
  });
  it('fetchOracleStoredProcData', () => {
    const taskEntity = {
      ID: 'string',
      TaskType: 'string',
      CreateDate: new Date(),
      Facility: 'string',
      CustID: 'string',
      Location: 'string',
      Priority: 'string',
    };

    controller.fetchOracleStoredProcData(taskEntity);
    expect(itemService.fetchOracleStoredProcData).toHaveBeenCalledWith(
      taskEntity,
    );
  });
  it('fetchPGStoredProcData', () => {
    controller.fetchPGStoredProcData();
    expect(itemService.fetchPGStoredProcData).toHaveBeenCalled();
  });

  it('fetchPGQueryData', () => {
    controller.fetchPGQueryData();
    expect(itemService.fetchPGQueryData).toHaveBeenCalled();
  });

  it('update', () => {
    controller.update(itemDto);
    expect(itemService.updateItem).toHaveBeenCalledWith(itemDto);
  });

  it('publish', () => {
    controller.publish(messageEntity);
    expect(itemService.publish).toHaveBeenCalledWith(messageEntity);
  });

  it('publishTopic', () => {
    controller.publishTopic(messageEntity);
    expect(itemService.publishTopic).toHaveBeenCalledWith(messageEntity);
  });

  it('publishDirect', () => {
    controller.publishDirect(messageEntity);
    expect(itemService.publishDirect).toHaveBeenCalledWith(messageEntity);
  });

  it('newTask', () => {
    controller.newTask(messageEntity);
    expect(itemService.newTask).toHaveBeenCalledWith(messageEntity);
  });

  it('remove', () => {
    controller.remove('id');
    expect(itemService.deleteItem).toHaveBeenCalledWith('id');
  });
});
