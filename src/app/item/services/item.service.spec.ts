import { Test, TestingModule } from '@nestjs/testing';
import { ItemService } from './item.service';
import {
  AppLoggerService,
  DomainApiService,
  OracleDBService,
  PostgressDBService,
  EmitService,
  EmitDirectService,
  EmitTopicService,
  NewTaskService,
} from 'synapse.bff.core';
import { HttpService } from '@nestjs/common';
import { DependencyUtlilizationService } from 'synapse.bff.core/dist/healthcheck/dependency-utlilization/dependency-utlilization.service';
import { ItemDto, MessageEntity } from '../dto';
import { of, Observable } from 'rxjs';
import { promises } from 'dns';

const itemDto: ItemDto = {
  Name: 'item1',
  Code: '100',
  Category: 'cat1',
  Price: 1250,
};
const itemDtoArray: ItemDto[] = [itemDto];
describe('ItemService', () => {
  let itemService: ItemService;
  let domainApiService: DomainApiService;
  let oracleDBService: OracleDBService;
  let postgressDBService: PostgressDBService;
  let emitDirectService: EmitDirectService;
  let emitTopicService: EmitTopicService;
  let newTaskService: NewTaskService;
  let emitService: EmitService;
  let appLoggerService: AppLoggerService;
  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [
        ItemService,
        {
          provide: AppLoggerService,
          useValue: {
            error: jest.fn(),
            warn: jest.fn(),
            log: jest.fn(),
            verbose: jest.fn(),
            debug: jest.fn(),
            getlogger: jest.fn(),
          },
        },
        {
          provide: DomainApiService,
          useValue: {
            post: jest.fn().mockReturnValue(of({ data: itemDtoArray })),
            get: jest.fn().mockReturnValue(of({ data: itemDtoArray })),
            put: jest.fn().mockReturnValue(of({ data: itemDtoArray })),
            delete: jest.fn().mockReturnValue(of({ data: itemDtoArray })),
          },
        },
        {
          provide: OracleDBService,
          useValue: {
            executeQuery: jest.fn().mockReturnValue({
              outBinds: {
                out_tasks: {
                  getRows: async (test: number = 100) => ['test'],
                },
              },
            })
            , getOracleConnection: Promise.resolve(),
          },
        },
        {
          provide: PostgressDBService,
          useValue: { executeQuery: jest.fn().mockReturnValue(null) },
        },
        {
          provide: EmitService,
          useValue: { publish: jest.fn() },
        },
        {
          provide: EmitDirectService,
          useValue: { publish: jest.fn() },
        },
        {
          provide: EmitTopicService,
          useValue: { publish: jest.fn() },
        },
        {
          provide: NewTaskService,
          useValue: { publish: jest.fn() },
        },

      ],
    }).compile();

    itemService = module.get<ItemService>(ItemService);
    domainApiService = module.get<DomainApiService>(DomainApiService);
    oracleDBService = module.get<OracleDBService>(OracleDBService);
    postgressDBService = module.get<PostgressDBService>(PostgressDBService);
    emitDirectService = module.get<EmitDirectService>(EmitDirectService);
    emitTopicService = module.get<EmitTopicService>(EmitTopicService);
    newTaskService = module.get<NewTaskService>(NewTaskService);
    emitService = module.get<EmitService>(EmitService);
    appLoggerService = module.get<AppLoggerService>(AppLoggerService);

  });

  it('should be defined', () => {
    expect(itemService).toBeDefined();
  });

  it('call getItems to be success and assert return value', () => {
    const path = '/item/getitem';
    itemService.getItems('').subscribe({
      next: (val: any) => {
        expect(domainApiService.get).toHaveBeenCalledWith(path);
        expect(val).toEqual(itemDtoArray);
      },
    });
  });

  it('call addItem to be success and assert return value', () => {
    const path = '/item/additem';
    itemService.addItem(itemDto).subscribe({
      next: (val: any) => {
        expect(domainApiService.post).toHaveBeenCalledWith(path, itemDto);
        expect(val).toEqual(itemDtoArray);
      },
    });
  });

  it('call updateItem to be success and assert return value', () => {
    const path = '/item/updateitem';
    itemService.updateItem(itemDto).subscribe({
      next: (val: any) => {
        expect(domainApiService.put).toHaveBeenCalledWith(path, itemDto);
        expect(val).toEqual(itemDtoArray);
      },
    });
  });

  it('call deleteItem to be success and assert return value', () => {
    const path = '/item/deleteitem';
    itemService.deleteItem('').subscribe({
      next: (val: any) => {
        expect(domainApiService.delete).toHaveBeenCalledWith(path, '');
        expect(val).toEqual(itemDtoArray);
      },
    });
  });

  it('call publish to be success', () => {
    const messageEntity: MessageEntity = {
      Name: 'synapsebffcore',
      Message: 'This is testing the rabbitMQ Publish',
    };

    itemService.publish(messageEntity);
    expect(emitService.publish).toHaveBeenCalledTimes(1);
  });

  it('call publishDirect to be success', () => {
    const messageEntity: MessageEntity = {
      Name: 'synapsebffcore',
      Message: 'This is testing the rabbitMQ publish Direct',
    };

    itemService.publishDirect(messageEntity);
    expect(emitDirectService.publish).toHaveBeenCalledTimes(1);
  });

  it('call publishTopic to be success', () => {
    const messageEntity: MessageEntity = {
      Name: 'synapsebffcore',
      Message: 'This is testing the rabbitMQ publish Topic',
    };

    itemService.publishTopic(messageEntity);
    expect(emitTopicService.publish).toHaveBeenCalledTimes(1);
  });

  it('call newTask to be success', () => {
    const messageEntity: MessageEntity = {
      Name: 'synapsebffcore',
      Message: 'This is testing the rabbitMQ Publish newTask',
    };

    itemService.newTask(messageEntity);
    expect(newTaskService.publish).toHaveBeenCalledTimes(1);
  });

  it('call fetchOracleQueryData to be success', () => {
    itemService.fetchOracleQueryData().then(result => {
      expect(oracleDBService.executeQuery).toHaveBeenCalledTimes(1);
    });
  });

  it('call fetchOracleStoredProcData to be success', () => {
    const taskEntity = {
      ID: 'string',
      TaskType: 'string',
      CreateDate: new Date(),
      Facility: 'string',
      CustID: 'string',
      Location: 'string',
      Priority: 'string',
    };

    itemService.fetchOracleStoredProcData(taskEntity).then(result => {
      expect(oracleDBService.executeQuery).toHaveBeenCalledTimes(1);
      expect(result).toEqual(['test']);
    });
  });
  it('call fetchOracleStoredProcData to be success', () => {
    itemService.fetchOracleStoredProcData(null).catch(result => {
      expect(appLoggerService.logger).toHaveBeenCalled();
    });
  });

  it('call fetchPGQueryData to be success', () => {
    itemService.fetchPGQueryData().then(result => {
      expect(result).toEqual(null);
      expect(postgressDBService.executeQuery).toHaveBeenCalledTimes(1);
    });
  });

  it('call fetchPGStoredProcData to be success', () => {
    itemService.fetchPGStoredProcData().then(result => {
      expect(result).toEqual(null);
      expect(postgressDBService.executeQuery).toHaveBeenCalledTimes(1);
    });
  });

});
