import { Injectable } from '@nestjs/common';
import oracledb from 'oracledb';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import {
  AppLoggerService,
  DomainApiService,
  EmitDirectService,
  EmitService,
  EmitTopicService,
  LogMessage,
  NewTaskService,
  OracleDBService,
  PostgressDBService,
} from 'synapse.bff.core';
import { ItemDto, MessageEntity, TaskEntity } from '../dto';

@Injectable()
export class ItemService {
  constructor(
    private oracleDBervice: OracleDBService,
    private postgressDBService: PostgressDBService,
    private domainApiService: DomainApiService,
    private emitService: EmitService,
    private emitDirectService: EmitDirectService,
    private emitTopicService: EmitTopicService,
    private newTaskService: NewTaskService,
    private appLoggerService: AppLoggerService,
  ) {}

  getItems(code: string): Observable<ItemDto[]> {
    const path = '/item/getitem';
    return this.domainApiService.get(path).pipe(
      map(resp => {
        return resp.data;
      }),
    );
  }
  getlargejson(): Observable<ItemDto[]> {
    const path = '/item/getlargejson';
    return this.domainApiService.get(path).pipe(
      map(resp => {
        return resp.data;
      }),
    );
  }
  async fetchOracleQueryData() {
    const dbResult = await this.oracleDBervice.executeQuery(
      'rfdest',
      'SELECT * FROM TASKS WHERE tasktype=:tasktype AND id=:id',
      ['PI', '934FBA351852BFE8E053839CCA0A94D8'],
    );
    return dbResult;
  }
  async fetchOracleStoredProcReturnCursor(taskEntity: TaskEntity) {
    return await this.oracleDBervice
      .executeStoredProcReturnCursor('rfdest', 'phy_gettask_r_p', {
        in_facility: taskEntity.Facility, // Bind type is determined from the data.  Default direction is BIND_IN
        in_custid: taskEntity.CustID,
        in_location: taskEntity.Location,
        out_tasks: { dir: oracledb.BIND_OUT, type: oracledb.CURSOR },
      })
      .then(result => {
        return result;
      });
  }

  async fetchOracleStoredProcData(taskEntity: TaskEntity) {
    let result: any;
    const numRows = 100;
    const dbResult = [];
    let rows;
    try {
      result = await this.oracleDBervice.executeQuery(
        'rfdest',
        `BEGIN
        phy_gettask_r_p(:in_facility, :in_custid, :in_location, :out_tasks);
    END;`,
        [
          {
            in_facility: taskEntity.Facility, // Bind type is determined from the data.  Default direction is BIND_IN
            in_custid: taskEntity.CustID,
            in_location: taskEntity.Location,
            out_tasks: { dir: oracledb.BIND_OUT, type: oracledb.CURSOR },
          },
        ],
      );

      do {
        rows = await result.outBinds.out_tasks.getRows(numRows); // get numRows rows at a time
        dbResult.push(...rows);
      } while (rows.length === numRows);

      return dbResult;
    } catch (error) {
      const message: LogMessage = {
        Title: 'This is unit testing',
        Type: 'Unit Test',
        Detail: error,
        Status: 'Open',
      };
      this.appLoggerService.log(message);
    }
  }

  async fetchPGStoredProcData() {
    const dbResult = await this.postgressDBService.executeQuery(
      'shipperanalyticsq',
      // 'SELECT * FROM get_shipper_request_by_id(\'355026f6-1896-4c82-953a-375f44e45952\')',
      'select * from get_shipper_request_summary(current_date,0,10)',
      [],
    );
    return dbResult;
  }

  async fetchPGQueryData() {
    const dbResult = await this.postgressDBService.executeQuery(
      'shipperanalyticsq',
      'SELECT * FROM "TESTMASTER"',
      [],
    );
    return dbResult;
  }

  async publish(messageEntity: MessageEntity) {
    this.emitService.publish(messageEntity.Name, messageEntity.Message);
  }
  async publishDirect(messageEntity: MessageEntity) {
    this.emitDirectService.publish(messageEntity.Name, messageEntity.Message);
  }
  async publishTopic(messageEntity: MessageEntity) {
    this.emitTopicService.publish(messageEntity.Name, messageEntity.Message);
  }
  async newTask(messageEntity: MessageEntity) {
    this.newTaskService.publish(messageEntity.Name, messageEntity.Message);
  }
  addItem(itemDto: ItemDto): Observable<ItemDto> {
    const path = '/item/additem';
    return this.domainApiService.post(path, itemDto).pipe(
      map(resp => {
        return resp.data;
      }),
    );
  }

  updateItem(itemDto: ItemDto): Observable<ItemDto[]> {
    const path = '/item/updateitem';
    return this.domainApiService.put(path, itemDto).pipe(
      map(resp => {
        return resp.data;
      }),
    );
  }

  deleteItem(code: string): Observable<ItemDto[]> {
    const path = '/item/deleteitem/' + code;
    return this.domainApiService.delete(path).pipe(
      map(resp => {
        return resp.data;
      }),
    );
  }
}
