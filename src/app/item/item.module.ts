import { Module } from '@nestjs/common';
import { ItemController } from './controller/item.controller';
import { ShipperlogEntity, TestMasterEntity } from './dto';
import { ItemService } from './services/item.service';

@Module({
  imports: [
    // CacheModule.register(
    //   {
    //   store: redisStore,
    //   host: 'localhost',
    //   port: 6379,
    // }),
  ],
  controllers: [ItemController],
  providers: [ItemService, ShipperlogEntity, TestMasterEntity],
  exports: [ShipperlogEntity, TestMasterEntity],
})
export class ItemModule {}
