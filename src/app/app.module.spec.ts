import { AppModule } from './app.module';
import { TestingModule, Test } from '@nestjs/testing';
import { AppLoggerService, LOGGER_MODULE_OPTIONS } from 'synapse.bff.core';

describe('App Module', () => {
  let appModule: AppModule;
  let appLoggerService: AppLoggerService;
  const loggerOptions = { appPath: process.cwd() };

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [
        AppModule,

        {
          provide: LOGGER_MODULE_OPTIONS,
          useValue: loggerOptions,
        },
        {
          provide: AppLoggerService,
          useValue: {
            log: jest.fn(),
          },
        },
      ],
    }).compile();

    appModule = module.get<AppModule>(AppModule);
    appLoggerService = module.get<AppLoggerService>(AppLoggerService);
  });

  it('should be defined', () => {
    expect(appModule).toBeDefined();
  });

  it('onModuleInit', () => {
    appModule.onModuleInit();
    expect(appLoggerService.log).toHaveBeenCalled();
  });
});
