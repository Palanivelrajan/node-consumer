import { Module, OnModuleInit, ValidationPipe } from '@nestjs/common';
import { APP_FILTER, APP_PIPE, APP_INTERCEPTOR } from '@nestjs/core';
import {
  AllExceptionsFilter,
  AppLoggerService,
  BadRequestExceptionFilter,
  HttpExceptionFilter,
  InterceptorOptions,
  RequestTimeoutExceptionFilter,
  RequestValidationPipe,
  SynapseBffCoreModule,
  LoggingInterceptor,
  TransformInterceptor,
  TimeoutInterceptor,
} from 'synapse.bff.core';
import { ItemModule } from './item/item.module';

const interceptorOptions: InterceptorOptions = new InterceptorOptions();
interceptorOptions.requestTimeout = [];
interceptorOptions.requestTimeout.push({ category: 'command', timeout: 10000 });
interceptorOptions.requestTimeout.push({ category: 'query', timeout: 12000 });
interceptorOptions.requestTimeout.push({
  category: 'aggregate',
  timeout: 15000,
});
@Module({
  imports: [
    ItemModule,
    SynapseBffCoreModule.forRoot({
      configOptions: { folder: '/src/config' },
    }),
  ],
  controllers: [],
  providers: [
    {
      provide: APP_INTERCEPTOR,
      useClass: LoggingInterceptor,
    },
    {
      provide: APP_INTERCEPTOR,
      useClass: TransformInterceptor,
    },
    {
      provide: APP_INTERCEPTOR,
      useClass: TimeoutInterceptor,
    },
    {
      provide: APP_FILTER,
      useClass: AllExceptionsFilter,
    },
    {
      provide: APP_FILTER,
      useClass: HttpExceptionFilter,
    },
    {
      provide: APP_FILTER,
      useClass: BadRequestExceptionFilter,
    },
    {
      provide: APP_FILTER,
      useClass: RequestTimeoutExceptionFilter,
    },
    {
      provide: APP_PIPE,
      useClass: ValidationPipe,
    },
    {
      provide: APP_PIPE,
      useClass: RequestValidationPipe,
    },
    // {
    //   provide: APP_INTERCEPTOR,
    //   useClass: CacheInterceptor,
    // },
  ],
})
export class AppModule implements OnModuleInit {
  constructor(private readonly logger: AppLoggerService) {}

  onModuleInit() {
    const message = {
      Title: 'Initialize constructor',
      Type: 'Info',
      Detail: 'Initialize constructor',
      Status: 'Status',
    };
    this.logger.log(message);
  }
}
