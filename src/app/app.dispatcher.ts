import { INestApplication, INestApplicationContext } from '@nestjs/common';
import { NestFactory } from '@nestjs/core';
import {
  FastifyAdapter,
  NestFastifyApplication,
} from '@nestjs/platform-fastify';
import { DocumentBuilder, SwaggerModule } from '@nestjs/swagger';
import helmet from 'helmet';
import { AppLoggerService } from 'synapse.bff.core';
import * as config from '../config/appconfig.json';
import { AppModule } from './app.module';

export class AppDispatcher {
  private app: INestApplication;
  private host = config.host;
  private port = config.port;
  constructor() {
    this.dispatch().catch(e => {
      process.exit(1);
    });
  }

  async dispatch(): Promise<void> {
    await this.createServer();
    return this.startServer();
  }

  async shutdown(): Promise<void> {
    await this.app.close();
  }

  public getContext(): Promise<INestApplicationContext> {
    return NestFactory.createApplicationContext(AppModule);
  }

  private async createServer(): Promise<void> {
    this.app = await NestFactory.create<NestFastifyApplication>(
      AppModule,
      new FastifyAdapter({
        logger: new AppLoggerService(),
      }),
    );

    this.app.enableCors();

    if (process.env.NODE_ENV === 'production') {
      this.app.use(helmet());
    }
    const options = new DocumentBuilder()
      .setTitle('Synapse BFF')
      .setDescription('A Backend for Frontend Web API developed in Node js')
      .setVersion(config.version)
      .addBearerAuth()
      .build();

    const document = SwaggerModule.createDocument(this.app, options);
    SwaggerModule.setup('/swagger', this.app, document);
  }

  private async startServer(): Promise<void> {
    await this.app.listen(this.port, this.host);
    const message = {
      Title: `Swagger is exposed at http://${this.host}:${this.port}/swagger`,
      Type: 'Info',
      Detail: 'Initialize constructor',
      Status: 'Status',
    };
    message.Title = `Server is listening http://${this.host}:${this.port}`;
  }
}
