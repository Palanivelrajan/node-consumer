import { Test, TestingModule } from '@nestjs/testing';
import request from 'supertest';
import { AppModule } from '../src/app/app.module';
import {
  FastifyAdapter,
  NestFastifyApplication,
} from '@nestjs/platform-fastify';

describe('AppController (e2e)', () => {
  let app;

  beforeEach(async () => {
    const moduleFixture: TestingModule = await Test.createTestingModule({
      imports: [AppModule],
    }).compile();
    app = moduleFixture.createNestApplication<NestFastifyApplication>(
      new FastifyAdapter(),
    );

    await app.init();
  });

  it('/healthcheck', () => {
    return app
    .inject({
      method: 'GET',
      url: '/health',
    })
     .then(( payload ) => expect(payload.statusCode).toBe(200));

    // return request(app.getHttpServer())
    //   .get('/health')
    //   .expect(200);
      //.expect('Hello World!');
  });

  afterAll(async () => {
    await app.close();
  });
});
