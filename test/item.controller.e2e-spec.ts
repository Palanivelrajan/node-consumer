import { Test, TestingModule } from '@nestjs/testing';
import request from 'supertest';
import { AppModule } from '../src/app/app.module';

import {
  FastifyAdapter,
  NestFastifyApplication,
} from '@nestjs/platform-fastify';
import { HttpService } from '@nestjs/common';
import { Observable } from 'rxjs';

describe('AppController (e2e)', () => {
  let app;
  let httpService: HttpService;
  beforeEach(async () => {
    const moduleFixture: TestingModule = await Test.createTestingModule({
      imports: [AppModule],
    }).compile();
    app = moduleFixture.createNestApplication<NestFastifyApplication>(
      new FastifyAdapter(),
    );
    httpService = moduleFixture.get<HttpService>(HttpService);
    await app.init();
    jest.setTimeout(30000);
  });

  it('/ (GET)', () => {
    const getMockResponse = [
      { name: 'item1', code: '100', category: 'cat1', price: 1250 },
      { name: 'item2', code: '101', category: 'cat1', price: 1250 },
      { name: 'item3', code: '102', category: 'cat1', price: 1250 },
      { name: 'item4', code: '103', category: 'cat1', price: 1250 },
    ];

    jest
      .spyOn(httpService, 'get')
      .mockReturnValue(Observable.create(getMockResponse));

    return app
      .inject({
        method: 'GET',
        url: '/item/100',
      })
      .then(payload => expect(JSON.parse(payload.body)).toEqual(getMockResponse));
  });

  it('/ (POST)', () => {
    const postMockParameter = {
      Name: 'item1',
      Code: '100',
      Category: 'cat1',
      Price: 1250,
    };

    jest
      .spyOn(httpService, 'post')
      .mockReturnValue(Observable.create(postMockParameter));

    return app
      .inject({
        method: 'POST',
        url: '/item/create',
        body: {
          postMockParameter,
        },
      })
      .then(payload =>
        expect(
          JSON.parse(payload.body).message[0].target.postMockParameter,
        ).toEqual(postMockParameter),
      );
  });

  it('/ (PUT)', () => {
    const postMockParameter = {
      Name: 'item1',
      Code: '100',
      Category: 'cat1',
      Price: 1250,
    };

    jest
      .spyOn(httpService, 'put')
      .mockReturnValue(Observable.create(postMockParameter));

    return app
      .inject({
        method: 'PUT',
        url: '/item/update',
        body: {
          postMockParameter,
        },
      })
      .then(payload =>
        expect(
          JSON.parse(payload.body).message[0].target.postMockParameter,
        ).toEqual(postMockParameter),
      );
  });

  it('/ (DELETE)', () => {
    const postMockParameter = {
      Code: '100',
    };

    jest
      .spyOn(httpService, 'delete')
      .mockReturnValue(Observable.create(postMockParameter));

    return app
      .inject({
        method: 'DELETE',
        url: '/item/100',
      })
      .then(payload =>
        expect(
          JSON.parse(payload.body),
        ).toEqual(postMockParameter),
      );
  });
});
